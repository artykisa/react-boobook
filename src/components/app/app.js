import React from "react";
import {Route, Switch, Redirect} from 'react-router-dom'
import HomePage from "../pages/home-page";
import CartPage from "../pages/cart-page";
import Header from "../header/header";
import BookPage from "../pages/book-page";
import LoginPage from "../pages/login-page";
import NotFound from "../not-found/not-found";

const App = () => {
    return (
        <main role='main' className='container'>
            <Header/>
            <Switch>
                <Route path='/' exact render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<HomePage />) : (<Redirect to="/login" />)
                }
                }/>
                <Route path='/cart' render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue? ((<CartPage/>) ): (<Redirect to="/login" />)
                }
                }/>
                <Route path='/book/:id' render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<BookPage />) : (<Redirect to="/login" />)
                }
                }/>
                <Route path='/login'  render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<Redirect to="/" />) : (<LoginPage/>)
                }
                }/>
                <Route component={NotFound} />
            </Switch>
        </main>
    )
}

export default App;

/*
                <Route path='/re-store/' exact render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<HomePage />) : (<Redirect to="/login" />)
                }
                }/>
                <Route path='/re-store/cart' render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<CartPage />) : (<Redirect to="/login" />)
                }
                }/>
                <Route path='/re-store/book/:id' render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<BookPage />) : (<Redirect to="/login" />)
                }
                }/>
                <Route path='/re-store/login'  render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<Redirect to="/" />) : (<LoginPage/>)
                }
                }/>
                <Route path='/re-store/order' render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<OrderPage />) : (<Redirect to="/login" />)
                }
                } />
 */


/*
<Route path='/' exact render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<HomePage />) : (<Redirect to="/login" />)
                }
                }/>
                <Route path='/cart' render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<CartPage />) : (<Redirect to="/login" />)
                }
                }/>
                <Route path='/book/:id' render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<BookPage />) : (<Redirect to="/login" />)
                }
                }/>
                <Route path='/login'  render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<Redirect to="/" />) : (<LoginPage/>)
                }
                }/>
                <Route path='/order' render = {() => {
                    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
                    return cookieValue?  (<OrderPage />) : (<Redirect to="/login" />)
                }
                } />
* */