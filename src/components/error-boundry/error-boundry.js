import React from "react";
import ErrorIndicator from "../error-indicator/error-indicator";

export default class ErrorBoundry extends React.Component {

    state = {
        hasError: false
    }

    render() {
        if(this.state.hasError){
            return <ErrorIndicator/>
        }
        return this.props.children
    }

    componentDidCatch(error, errorInfo) {
        this.setState({hasError: true})
    }
}