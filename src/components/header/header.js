import React, {useContext, useEffect} from "react";
import './header.css'
import {Link, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import BookstoreServiceContext from "../bookstore-service-context/bookstore-service-context";
import {fetchCart} from "../../actions";
const Header = () => {
    const history = useHistory();
    const dispatch = useDispatch()
    const isLoggedIn = useSelector((state) => state.isLoggedIn)
    const {itemsTotal, showCartPage} = useSelector((state) => state)

    const logout = () => {
        dispatch({
            type: "NOT_LOGGED_IN"
        })
        document.cookie = "token=; max-age=-1";
        history.push("/")
    }

    const bookstoreService = useContext(BookstoreServiceContext)

    useEffect(() => {
        if(isLoggedIn){
            fetchCart(dispatch, bookstoreService)();
        }
    }, [isLoggedIn])

    return(
        <header className='shop-header'>
            <Link to='/' className='logo text-dark'>BooBook</Link>
            {
                isLoggedIn && showCartPage &&
                <React.Fragment>
                    <button onClick={logout} className='btn-logout btn btn-outline-secondary'>Logout</button>
                    <Link to={"/cart"} className='shopping-cart'>
                        <i className="fas fa-cart-arrow-down"></i>{ itemsTotal} books
                    </Link>
                </React.Fragment>
            }
        </header>
    )
}

export default Header