import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./store";
import App from "./components/app/app";
import ErrorBoundry from "./components/error-boundry/error-boundry";
import BookstoreService from "./services/bookstore-service";
import BookstoreServiceContext from "./components/bookstore-service-context/bookstore-service-context";

const bookstoreService = new BookstoreService();

ReactDOM.render(
    <Provider store={store}>
        <ErrorBoundry>
            <BookstoreServiceContext.Provider value={bookstoreService}>
                <HashRouter basename="/">
                    <App/>
                </HashRouter>
            </BookstoreServiceContext.Provider>
        </ErrorBoundry>
    </Provider>,
    document.getElementById('root'));

//basename="/re-store"