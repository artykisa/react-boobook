import React from "react";
const ErrorIndicator = () => {
    return (
        <p>Oops! Something went wrong :(</p>
    )
}
export default ErrorIndicator;