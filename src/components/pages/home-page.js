import React from "react";
import BookListContainer from "../book-list/book-list";

const HomePage = () => {
    return(
    <React.Fragment>
        <BookListContainer/>
    </React.Fragment>
    )
}
export default HomePage;