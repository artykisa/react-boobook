import $ from "jquery";

export default class BookstoreService {
    _apiBase = "https://ake.qulix.com"
    //_apiBase1 = 'https://api.nytimes.com/svc/books/v3/lists/overview.json?api-key=bPoUkLRWWI9j91paAqm1VXXGv4MEkBHS'

    /*getBooks = async () => {
        const res = await fetch(this._apiBase1)
        if (!res.ok){
            throw new Error(`could not fetch ${this._apiBase}, received ${res.status}`)
        }
        const body = await res.json();
        const books = body.results.lists[0].books;
        console.log(body)
        return books
    }*/


    login = (username, password) => {
        return new Promise(function (resolve, reject){
            $.ajax({
                url: 'https://ake.qulix.com/api/users/login',
                method: 'POST',
                dataType: 'html',
                data: {email:username, password:password},
                success: function(response){
                    const data = JSON.parse(response)
                    document.cookie = "token=Bearer " + data.accessToken;
                    resolve(response)
                },
                error: function (response){
                    reject(response)
                }
            });
        })
    }
    register = (username, password) => {
        return new Promise(function (resolve, reject){
            $.ajax({
                url: 'https://ake.qulix.com/api/users',
                method: 'POST',
                dataType: 'html',
                data: {email:username, password:password},
                success: function(response){
                    const data = JSON.parse(response)
                    resolve(data)
                },
                error: function (response){
                    reject(response)
                }
            });
        })
    }

    getBook = (id) => {
        return new Promise(function (resolve, reject) {
            let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
            $.ajax({
                url: 'https://ake.qulix.com/api/books/'+id,
                method: 'Get',
                dataType: 'html',
                headers: {"Authorization": cookieValue},
                success: function(response){
                    const data = JSON.parse(response)
                    resolve(data)
                },
                error: function (response){
                    reject(response)
                }
            });
        })
    }

    getBookss = () => {
        return new Promise(function (resolve, reject) {
            let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
            $.ajax({
                url: 'https://ake.qulix.com/api/books',
                method: 'Get',
                dataType: 'html',
                headers: {"Authorization": cookieValue},
                success: function(response){
                    const data = JSON.parse(response)
                    resolve(data)
                },
                error: function (response){
                    reject(response)
                }
            });
        })
    }

    getCart = () => {
        return new Promise(function (resolve, reject) {
            let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
            $.ajax({
                url: 'https://ake.qulix.com/api/cart',
                method: 'Get',
                dataType: 'html',
                headers: {"Authorization": cookieValue},
                success: function(response){
                    const data = JSON.parse(response)
                    resolve(data)
                },
                error: function (response){
                    reject(response)
                }
            });
        })
    }

    addToCart = (id) => {
        return new Promise(function (resolve, reject) {
            let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
            $.ajax({
                url: 'https://ake.qulix.com/api/cart/'+id,
                method: 'Post',
                dataType: 'html',
                headers: {"Authorization": cookieValue},
                success: function(response){
                    const data = JSON.parse(response)
                    resolve(data)
                },
                error: function (response){
                    reject(response)
                }
            });
        })
    }

    getCurrentUser = () => {
        return new Promise(function (resolve, reject) {
            let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
            $.ajax({
                url: 'https://ake.qulix.com/api/users/user',
                method: 'GET',
                dataType: 'html',
                headers: {"Authorization": cookieValue},
                success: function(response){
                    const data = JSON.parse(response)
                    resolve(data)
                },
                error: function (response){
                    reject(response)
                }
            });
        })
    }

    deleteAllBooksId = (id) => {
        return new Promise(function (resolve, reject) {
            let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
            $.ajax({
                url: 'https://ake.qulix.com/api/cart/remove/'+id,
                method: 'DELETE',
                dataType: 'html',
                headers: {"Authorization": cookieValue},
                success: function(response){
                    const data = JSON.parse(response)
                    resolve(data)
                },
                error: function (response){
                    reject(response)
                }
            });
        })
    }
    deleteBookId = (id) => {
        return new Promise(function (resolve, reject) {
            let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
            $.ajax({
                url: 'https://ake.qulix.com/api/cart/'+id,
                method: 'DELETE',
                dataType: 'html',
                headers: {"Authorization": cookieValue},
                success: function(response){
                    const data = JSON.parse(response)
                    resolve(data)
                },
                error: function (response){
                    reject(response)
                }
            });
        })
    }

    postOrder = (bonuses, name, phone, city, street, house, flat, code) => {
        return new Promise(function (resolve, reject) {
            let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1");
            $.ajax({
                url: 'https://ake.qulix.com/api/cart/order',
                method: 'POST',
                dataType: 'html',
                headers: {"Authorization": cookieValue},
                data: {
                    bonuses:bonuses,
                    name: name,
                    phone: phone,
                    address: {
                        city: city,
                        street: street,
                        house: house,
                        flat: flat,
                        code: code
                    }
                },
                success: function(response){
                    const data = JSON.parse(response)
                    //console.log(data, "post cart");
                    resolve(data)
                },
                error: function (response){
                    reject(response)
                }
            });
        })
    }
}