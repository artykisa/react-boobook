import React, {useContext, useRef, useState} from "react";
import BookstoreServiceContext from "../bookstore-service-context/bookstore-service-context";
import { useHistory } from "react-router-dom";
import {useDispatch} from "react-redux";
import "./login-page.css"

const LoginPage = () => {
    const dispatch = useDispatch()
    const [errorMsg, setErrorMsg] = useState("")
    let usernameRef = useRef()
    let passwordRef = useRef()
    const history = useHistory()
    const bookstoreService = useContext(BookstoreServiceContext)

    const login = async (e) => {
        e.preventDefault()
        const username  = usernameRef.current.value
        const password = passwordRef.current.value
        try{
           const result = await bookstoreService.login(username, password)
            //console.log(result)
            dispatch({
                type: "IS_LOGGED_IN"
            })
            history.push("/")
        }
        catch (response) {
            //console.log(response)
            setErrorMsg("Wrong password or login")
        }
    }

    const register = async (e) => {
        e.preventDefault()
        const username  = usernameRef.current.value
        const password = passwordRef.current.value
        try{
            const result = await bookstoreService.register(username, password)
            setErrorMsg("Registration completed successfully. Please, login now")
        }
        catch (response) {
            //console.log(response)
            //console.log("User with this email already exists")
            setErrorMsg("User with this email already exists or invalid input")
        }
    }



    return(
        <div className="col-md-6 col-md-offset-3">
            <h2>Login</h2>
            <form name="form">
                <div className={'form-group'}>
                    <label htmlFor="username">Email</label>
                    <input required ref={usernameRef} type="email" className="form-control" name="username" />
                </div>
                <div className={'form-group'}>
                    <label htmlFor="password">Password</label>
                    <input required ref={passwordRef} type="password" className="form-control" name="password" />
                </div>
                <span>{errorMsg}</span>
                <div className="form-group btns">
                    <button onClick={login} className="btn btn-primary">Login</button>
                    <button onClick={register} className="btn ">Register</button>
                </div>
            </form>
        </div>
    )
}

export default LoginPage