const initialState = {
    books: [],
    loading: true,
    error: null,
    cartItems:[],
    orderTotal: 0,
    itemsTotal: "",
    updateCartPage: false,
    showCartPage: true,
    isLoggedIn: Boolean(document.cookie.replace(/(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/, "$1"))
}

const updateBookInCart = (id, state, typeOfUpdate) => {
    const bookInCart = state.books.find(book => {
        return book.id === +id})

    const sameItemIndex = state.cartItems.findIndex(item => item.id === id)
    const sameItem = state.cartItems[sameItemIndex]
    let newItem;
    if (sameItem) {
        newItem = {
            ...sameItem,
            count: sameItem.count + typeOfUpdate,
            price: sameItem.price,
            totalPrice: Number(sameItem.totalPrice + (typeOfUpdate * sameItem.price))
        }

        if (newItem.count === 0){

            return {...state,
                cartItems: [
                    ...state.cartItems.slice(0, sameItemIndex),
                    ...state.cartItems.slice(sameItemIndex+1)
                ]}
        }
        return {
            ...state,
            cartItems: [
                ...state.cartItems.slice(0, sameItemIndex),
                newItem,
                ...state.cartItems.slice(sameItemIndex+1)
            ]
        }
    }
    newItem = {
        id: id,
        name: bookInCart.name,
        count: 1,
        price: bookInCart.price,
        totalPrice: bookInCart.price
    }

    return {
        ...state,
        cartItems: [
            ...state.cartItems,
            newItem
        ]
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type){
        case 'FETCH_BOOKS_SUCCESS':
            //console.log(action.payload)
            return {
                ...state,
                books: action.payload,
                loading: false,
                error: null
            }

        case 'FETCH_BOOKS_REQUEST':
            return {
                ...state,
                books: [],
                loading: true,
                error: null
            }

        case 'SHOW_CART_PAGE':
            return {
                ...state,
                showCartPage: !state.showCartPage
            }

        case 'UPDATE_CART_PAGE':
            return {
                ...state,
                updateCartPage: true
            }

        case 'NOT_UPDATE_CART_PAGE':
            return {
                ...state,
                updateCartPage: false
            }

        case 'FETCH_BOOKS_FAILURE':
            return {
                ...state,
                books: [],
                loading: false,
                error: action.payload
            }

        case 'BOOK_DELETED_FROM_CART':
            return updateBookInCart(action.payload, state, -1)

// book - rank, item - id
        case 'BOOK_ADDED_TO_CART': //146 урок рефактор
            return updateBookInCart(action.payload, state, 1)

        case 'ALL_BOOKS_DELETED_FROM_CART':
            const item = state.cartItems.find(item => item.id === action.payload)
            return updateBookInCart(action.payload, state, -item.count)

        case 'BOOKS_CHANGED':
            return {
                ...state,
                itemsTotal: state.cartItems.reduce((sum, item ) => Number(sum + item.count), 0),
                orderTotal: state.cartItems.reduce((sum, item ) => Number(sum + item.totalPrice), 0)
            }

        case 'IS_LOGGED_IN':
            return {
                ...state,
                isLoggedIn: true
            }

        case "FETCH_CART_SUCCESS":
            //console.log("CAART")
            return {
                ...state,
                cartItems: action.payload,
                itemsTotal: action.payload.books.reduce((sum, book) => sum+book.count , 0)
            }

        case 'NOT_LOGGED_IN':
            return {
                ...state,
                isLoggedIn: false
            }

        default:
            return state;
    }
}

export default reducer;