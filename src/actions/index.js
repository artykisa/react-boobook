const booksLoaded = (newBooks) => {
    return{
        type: 'FETCH_BOOKS_SUCCESS',
        payload: newBooks
    }
}

const cartLoaded = (booksInCart) => {
    return{
        type: 'FETCH_CART_SUCCESS',
        payload: booksInCart
    }
}

const booksRequested = () => {
    return{
        type: 'FETCH_BOOKS_REQUEST'
    }
}

const updateCartPageAction = () => {
    return{
        type: 'UPDATE_CART_PAGE'
    }
}

const notUpdateCartPage = () => {
    return{
        type: 'NOT_UPDATE_CART_PAGE'
    }
}


const booksError = (error) => {
    return{
        type: 'FETCH_BOOKS_FAILURE',
        payload: error
    }
}

const bookAddedToCart = (id) => {
    return{
        type: 'BOOK_ADDED_TO_CART',
        payload: id
    }
}

const allBooksDeletedFromCart = (id) => {
    return{
        type: 'ALL_BOOKS_DELETED_FROM_CART',
        payload: id
    }
}

const fetchBooks =  (dispatch, bookstoreService) => async () => {
    dispatch(booksRequested())
    try{
        const books = await bookstoreService.getBookss()
        //console.log(books, "books")
        dispatch(booksLoaded(books))
    }
    catch (error){
        dispatch(booksError(error))
        console.error(error)
    }
}

const fetchCart =  (dispatch, bookstoreService) => async () => {
   // dispatch(booksRequested())
    try{
        const books = await bookstoreService.getCart()
        dispatch(cartLoaded(books))
    }
    catch (error){
        //dispatch(booksError(error))
        //console.error(error)
    }
}

export {allBooksDeletedFromCart, booksLoaded, booksRequested,
    booksError, fetchBooks, bookAddedToCart, fetchCart, cartLoaded, updateCartPageAction, notUpdateCartPage}