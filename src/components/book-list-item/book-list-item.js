import React from "react";
import {withRouter} from 'react-router-dom';
import './book-list-item.css'
const BookListItem = ({book, onAddedToCart, history}) => {
    const {name: bookName = "Name of book", price, image, id, author: {name} = {}} = book;
    return(
        <div className='book-list-item'>
            <div className='book-cover'>
                <img src={"data:image/png;base64,"+image} alt={bookName}/>
            </div>
            <div className='book-details'>
                <a href=''
                   onClick={(e) => {
                    e.preventDefault()
                    history.push(`/book/${id}`)
                    }}
                   className='book-title'>{bookName}</a>
                <span className='book-author'>{name}</span>
                <span className='book-price'>{price} $</span>
                <button className='btn btn-outline-primary add-to-cart'
                        onClick={onAddedToCart}>Add to cart</button>
            </div>
        </div>
    )
}
export default withRouter(BookListItem)