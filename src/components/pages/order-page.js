import React, {useContext, useLayoutEffect, useRef, useState} from "react";
import "./order-page.css"
import {Link} from "react-router-dom";
import BookstoreServiceContext from "../bookstore-service-context/bookstore-service-context";
import {cartLoaded } from "../../actions";
import {useDispatch} from "react-redux";

const OrderPage = ({bonuses, backToCart}) => {

    const [successfulOrder,setSuccessfulOrder] = useState(false)
    const [isCartEmtpty,setIsCartEmpty] = useState(false)
    const [isValid,setIsValid] = useState(true)
    const bookstoreService = useContext(BookstoreServiceContext)
    const nameRef = useRef()
    const phoneRef = useRef()
    const cityRef = useRef()
    const streetRef = useRef()
    const houseRef = useRef()
    const flatRef = useRef()
    const codeRef = useRef()
    const dispatch = useDispatch()

    useLayoutEffect(()=>{
        dispatch({type: "SHOW_CART_PAGE"})
        return () => dispatch({type: "SHOW_CART_PAGE"})
    }, [])

    const onSubmitOrder = () => {
        //console.log(streetRef.current.value.trim())
        if(phoneRef.current.value.trim() && nameRef.current.value.trim() && streetRef.current.value.trim()
        && flatRef.current.value.trim() && houseRef.current.value.trim()  && cityRef.current.value.trim() && codeRef.current.value.trim())
        {
            setIsValid(true)
            //console.log(bonuses)
            bookstoreService.getCart().then((data) => {
                if(!data.books.length){
                    setIsCartEmpty(true)
                }
                else{
                    bookstoreService.postOrder(bonuses, nameRef.current.value, phoneRef.current.value,
                        cityRef.current.value, streetRef.current.value, houseRef.current.value,
                        flatRef.current.value, codeRef.current.value).then((data) => {
                            dispatch(cartLoaded(data))
                            setSuccessfulOrder(true)
                        }
                    )
                }
            })
        }
        else{
            setIsValid(false)
        }
    }

    if(successfulOrder){
        return (
            <React.Fragment>
                <p>Thank you for order! Our manager will contact you soon</p>
                <Link to="/">Go to main page</Link>
            </React.Fragment>
        )
    }
    if(isCartEmtpty){
        return (
            <React.Fragment>
                <p className={"error-message"}>Oops! Your shopping cart is empty!</p>
                <Link to="/">Go to main page</Link>
            </React.Fragment>
        )
    }
    return(
    <div className="row">
        <div className="order col-75">
            <div className="container">
                    <div className="row">
                        <div className="col-50 order">
                            <h3>Order Information</h3>
                            <a href="#" onClick={backToCart}>Go back to cart</a>
                            <label htmlFor="fname"><i className="fa fa-user"></i> Full Name</label>
                            <input ref={nameRef} type="text" id="fname" name="name" placeholder="John M. Doe"/>
                            <label htmlFor="ph"><i className="fa fa-mobile" aria-hidden="true"></i> Mobile Phone</label>
                            <input ref={phoneRef} type="tel" id="ph" name="phone" placeholder="+ 44 4444 4444"/>
                            <label htmlFor="city"><i className="fas fa-city"></i> City</label>
                            <input ref={cityRef} type="text" id="city" name="city" placeholder="London"/>
                            <label htmlFor="street"><i className="fas fa-road"></i> Street</label>
                            <input ref={streetRef} type="text" id="street" name="street" placeholder="Baker Street"/>
                            <label htmlFor="house"><i className="fa fa-home" aria-hidden="true"></i> House</label>
                            <input ref={houseRef} type="text" id="house" name="house" placeholder="1"/>
                            <label htmlFor="flat"><i className="fas fa-door-closed"></i> Flat</label>
                            <input ref={flatRef} type="text" id="flat" name="flat" placeholder="221b"/>
                            <label htmlFor="code"><i className="fas fa-envelope"></i> Code</label>
                            <input ref={codeRef} type="text" id="code" name="code" placeholder="90210"/>
                        </div>
                    </div>
                {
                    isValid?"":<p className={"error-message"}>All fields are required</p>
                }
                    <button onClick={onSubmitOrder} className="btn btn-success btn-submit-order">Submit</button>
            </div>
        </div>
    </div>
    )
}

export default OrderPage