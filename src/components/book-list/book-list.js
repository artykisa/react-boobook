import React, {useContext, useLayoutEffect} from "react";
import BookListItem from "../book-list-item/book-list-item";
import {useDispatch, useSelector} from "react-redux";
import BookstoreServiceContext from "../bookstore-service-context/bookstore-service-context";
import {cartLoaded, fetchBooks} from "../../actions";
import './book-list.css'
import Spinner from "../spinner/spinner";
import ErrorIndicator from "../error-indicator/error-indicator";

const BookListContainer = () => {
    const dispatch = useDispatch()
    const bookstoreService = useContext(BookstoreServiceContext)

    useLayoutEffect(() => {
        fetchBooks(dispatch, bookstoreService)();
    }, [bookstoreService,dispatch])

    const {books, loading, error} = useSelector((state) => state)
    if (loading) return <Spinner/>
    if (error) return <ErrorIndicator/>

    return <BookList books={books} onAddedToCart={(id) =>{
        bookstoreService.addToCart(id).then((data) => {
            dispatch(cartLoaded(data))
        })}
    }/>
}

const BookList = ({books, onAddedToCart}) => {
    return(
        <ul className='book-list'>
            {
                books.map((book) => {
                    return (
                        <li key={book.id}><BookListItem book={book}
                                                        onAddedToCart={() => onAddedToCart(book.id)}/>
                        </li>
                    )
                })
            }
        </ul>
    )
}


export default BookListContainer;