import React from "react";
import {Link} from "react-router-dom";
const NotFound = () => {
    return(
        <div>
            <p>Oops! Not found!</p>
            <Link to="/">Go Home</Link>
        </div>
    )
}

export default NotFound