import React, {useContext, useLayoutEffect, useState} from "react";
import {useParams} from 'react-router-dom'
import BookstoreServiceContext from "../bookstore-service-context/bookstore-service-context";
import Spinner from "../spinner/spinner";
import ErrorIndicator from "../error-indicator/error-indicator";
import {cartLoaded} from "../../actions";
import {useDispatch} from "react-redux";
import "./book-page.css"
const BookPage = () => {

    let { id } = useParams();
    const [book, setBook] = useState({})
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const dispatch = useDispatch()
    const bookstoreService = useContext(BookstoreServiceContext)
    const getBookInfo = async (id) => {
        try{
            setLoading(true)
            const result = await bookstoreService.getBook(id)
            setLoading(false)
            setError(false)
            return result
        }
        catch (response) {
            setError(true)
            //console.log(response)
        }
    }
    useLayoutEffect(() => {
        getBookInfo(id).then((data) => setBook(data))
    }, [])

    const {name: bookName, description, price, image, author: {name} = {}} = book
    if (loading) return <Spinner/>
    if (error) return <ErrorIndicator/>

    return(
        <div className='book-list-item'>
            <div className='book-cover'>
                <img src={"data:image/png;base64,"+image} alt={bookName}/>
            </div>
            <div className='book-details'>
                <h3 className='book-title'>{bookName}</h3>
                <h6 className='book-author'>{name}</h6>
                <span className='book-description'>{description}</span>
                <span className='book-price book-page-price'>{price}$</span>
                <button className='btn btn-outline-primary add-to-cart'
                        onClick={() =>{
                            //console.log(id)
                            bookstoreService.addToCart(id).then((data) => {
                                dispatch(cartLoaded(data))
                            })
                    //dispatch(bookAddedToCart(id))
                    //dispatch(booksChanged())
                }}>Add to cart</button>
            </div>
        </div>
    )
}

export default BookPage
