import React, {useContext, useLayoutEffect, useRef, useState} from "react";
import './shopping-cart.css'
import {useDispatch} from "react-redux";
import {cartLoaded} from "../../actions";
import BookstoreServiceContext from "../bookstore-service-context/bookstore-service-context";
import NumericInput from 'react-numeric-input';
import OrderPage from "../pages/order-page";

const ShoppingCart = () => {
    const [confirmation ,setConfirmation] = useState(false)
    const inputRef = useRef();
    const dispatch = useDispatch();
    const [total, setTotal] = useState(0)
    const [totalWithBonuses, setTotalWithBonuses] = useState(0)
    const [bonuses, setBonuses] = useState(0)
    const [books, setBooks] = useState([])
    const [errorMessage, setErrorMessage] = useState("")
    const [loading, setLoading] = useState(true)
    const [bonusesOverflow ,setBonusesOverflow] = useState(false)

    const bookstoreService = useContext(BookstoreServiceContext)

    useLayoutEffect(() => {
        bookstoreService.getCart().then((data) => {
            setTotal(data.books.reduce((sum, book) => book.count*book.price + sum,0))
            setBooks(data.books)
            setLoading(false)
        })
        bookstoreService.getCurrentUser().then((data) => {
            setBonuses(data.bonuses)
        })
    }, [])

    const deleteAllBooksId = (id) => {
        bookstoreService.deleteAllBooksId(id).then((data) => {
            setBooks(data.books)
            dispatch(cartLoaded(data))
            setTotal(data.books.reduce((sum, book) => book.count*book.price + sum,0))
        })
    }

    const deleteBookId = (id) => {
        bookstoreService.deleteBookId(id).then((data) => {
            setBooks(data.books)
            dispatch(cartLoaded(data))
            setTotal(data.books.reduce((sum, book) => book.count*book.price + sum,0))
        })
    }

    const addToCart = (id) => {
        bookstoreService.addToCart(id).then((data) => {
            setBooks(data.books)
            dispatch(cartLoaded(data))
            setTotal(data.books.reduce((sum, book) => book.count*book.price + sum,0))
        })
    }

    const backToCart = (e) => {
        e.preventDefault()
        setConfirmation(false)
        setErrorMessage("")
        setTotalWithBonuses(0)
    }

    const renderRow = (item, index) => {
        const {id, name, count, price}= item
        return(
            <tr key={id}>
                <td>{index + 1}</td>
                <td>{name}</td>
                <td>{count}</td>
                <td>{price * count}$</td>
                <td>
                    <button type="button" onClick={() => {
                        //dispatch(bookAddedToCart(id))
                        //dispatch(booksChanged())
                        addToCart(id)
                    }}
                            className="btn btn-outline-success">
                        <i className="fas fa-plus-circle"></i>
                    </button>
                    <button type="button" onClick={() => {
                        //dispatch(allBooksDeletedFromCart(id))
                        //dispatch(booksChanged())
                        deleteAllBooksId(id)
                    }}
                            className="btn btn-outline-danger">
                        <i className="fas fa-trash-alt"></i>
                    </button>
                    <button type="button" onClick={() => {
                        //dispatch(bookDeletedFromCart(id))
                        //dispatch(booksChanged())
                        deleteBookId(id)
                    }}
                            className="btn btn-outline-warning">
                        <i className="fas fa-minus-circle"></i>
                    </button>
                </td>
            </tr>
        )
    }
    const buyBtnClick = (e) => {
        if (inputRef.current.state.value <= total * 0.3){
            setConfirmation(true)
        }
        if (!inputRef.current.state.value){
            e.preventDefault();
            setBonusesOverflow(true)
            setErrorMessage("Input number of bonuses")
            return
        }
        else{
            e.preventDefault();
            setBonusesOverflow(true)
            setErrorMessage("You can't use this number of bonuses. You can pay up to 30% of the total cost")
        }
    }

    if(confirmation){
        return <OrderPage bonuses={inputRef.current.state.value} backToCart={backToCart}/>
    }


    const onBonusChange = (value) => {
        if(!value){
            setTotalWithBonuses(0)
            return
        }
        setTotalWithBonuses(total-value)
    }

    return(
        <div className='shopping-cart'>
            <h2>Your order</h2>
            {loading? <span>Loading...</span>:
                books.length?
                    <React.Fragment>
                        <table className='table'>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Count</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                books.map(renderRow)
                            }
                            </tbody>
                        </table>
                        <p>You have <b>{bonuses.toFixed(2)}</b> bonuses, you can use <b>{bonuses > total * 0.3?Math.floor(total * 0.3): Math.floor(bonuses)}</b> bonuses</p>
                        <p>How much bonuses do you want to use?</p>
                        <NumericInput ref={inputRef}  min={ 0 } max = {bonuses > total * 0.3?Math.floor(total * 0.3): Math.floor(bonuses)}
                                      maxLength = {bonuses.toFixed(2).length - 2} step={ 1 } precision={ 0 } size={ 4 } strict
                                      onChange = {onBonusChange}
                            //parse={parse}
                        />
                        {
                            bonusesOverflow? <p className={"error-message"}>{errorMessage}</p>:""
                        }
                        <div className='total'>
                            <b>Total</b>: {totalWithBonuses? totalWithBonuses: total}$
                            {total? <button onClick={buyBtnClick} className='btn-buy btn btn-primary'>Buy</button>:''}
                        </div>
                    </React.Fragment>
                    : <span>No books here</span>}
        </div>
    )
}

export default ShoppingCart;